import array
import sys
import config

def ampli (sound, factor:float):
    nsamples= config.samples_second*int(sys.argv[4])
    sound=array.array('h',[0]*nsamples)
    if factor>0:

        for nsample in range (nsamples):

            if sound[nsample]> config.max_amp:
                sound[nsample]=config.max_amp
            elif sound[nsample]< -config.max_amp:
                sound[nsample]=-config.max_amp
            else:
                sound[nsample]*=factor
        return sound
    else:
        print("ERROR, EL FACTOR TIENE QUE SER POSITIVO")
def reduce(sound,factor:int):
    nsamples=config.samples_second*int(sys.argv[4])
    sound=array.array('h',[0]*nsamples)

    if factor>0:
        del sound[::factor]

        return sound
    else:
        print ('ERROR, EL FACTOR TIENE QUE SER POSITIVO')

def extend (sound, factor:int):
    nsamples=config.samples_second*int(sys.argv[4])
    n=0
    if factor >0:
        for i in range (factor,nsamples,factor):
            if i>factor:
                n+=1
                i=i+n
                sound.insert(i,(sound[i-1])+sound[i]//2)
            else:
                sound.insert(i,(sound[i-1]+sound[i]//2))

        return sound

    else:
        print("ERROR, EL FACTOR TIENE QUE SER POSITIVO")


